package cn.uncode.schedule.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;


/**
 * @author juny.ye
 */
@Component
public class SimpleTask {

    private static int i = 0;
    

    public void print() {
        System.out.println("===========start!=========");
        System.out.println("I:"+i);i++;
        System.out.println("=========== end !=========");
    }
    
    public void print1() {
        System.out.println("===========start!=========");
        System.out.println("print<<1>>:"+i);i++;
        System.out.println("=========== end !=========");
    }
    
    public void print2() {
        System.out.println("===========start!=========");
        System.out.println("print<<2>>:"+i);i++;
        System.out.println("=========== end !=========");
    }
    
    public void print3() {
        System.out.println("===========start!=========");
        System.out.println("print<<3>>:"+i);i++;
        System.out.println("=========== end !=========");
    }
    
    public void print4() {
        System.out.println("===========start!=========");
        System.out.println("print<<4>>:"+i);i++;
        System.out.println("=========== end !=========");
    }
    
    
    public void print5(String param) {
        System.out.println("===========start!=========");
        System.out.println("print<<5>>:"+i+"-"+param);i++;
        System.out.println("=========== end !=========");
    }
    
    
    /**
     * 分布式任务示例
     * @return
     */
    
    /**
     * 分布式任务示例-before方法
     * @param params 可选，有参时必须是String类型
     * @return List类型对象，List<User>也可以
     */
    public List<Map<String, String>> before(String params){
    	
    	List<Map<String, String>> list = new ArrayList<>();
    	for(int a = 4100;a <= 5120; a++){
    		String key = a + "ksudi";
    		Map<String, String> item = new HashMap<>();
    		for(int i=100;i<150;i++){
    			String value = key + i;
    			item.put(value, value);
        	}
//    		System.err.println(item);
    		list.add(item);
    	}
    	return list;
    }
    

    /**
     * 分布式任务示例-目标执行方法
     * @param must 必有项，before方法返回的list item给以json字符串的行形传到该参数中来，需要自已解析
     * @param params 可选，有参时必须是String类型
     */
    public void runing(String must, String params){
    	System.err.println(must);
    }
    
    /**
     * 分布式任务示例-after方法
     * @param params 可选，有参时必须是String类型
     */
    public void after(String params){
    	System.err.println("after====================================================");
    }


}
